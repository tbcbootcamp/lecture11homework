package com.example.lecture11homework

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*


class User {
    var name = "Giga"
    var surname = "Sulkhanishvili"
    var email = "sulkhanishviligiga3@gmail.com"
    var year = 2000
    var gender = "Male"
}

class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 69
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        editProfileInfo(User().name, User().surname, User().email, User().year, User().gender)
        editInfoButton.setOnClickListener {
            changeProfileInfo()
        }
    }

    private fun changeProfileInfo() {
        val intent = Intent(this, EditInfoActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    private fun editProfileInfo(
        name: String,
        surname: String,
        email: String,
        birthdayYear: Int,
        gender: String
    ) {
        nameTextView.text = "$name"
        surnameTextView.text = "$surname"
        emailTextView.text = "Email: $email"
        birthYearTextView.text = "Birth year: ${birthdayYear.toString()}"
        genderTextView.text = "Gender: $gender"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            val userModel = data?.extras!!.get("userModel") as UserModel
            editProfileInfo(
                userModel.name,
                userModel.surname,
                userModel.email,
                userModel.birthdayYear,
                userModel.gender
            )
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}